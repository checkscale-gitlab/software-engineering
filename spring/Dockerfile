# Base image
FROM alpine

# Image labels
LABEL maintainer="Thiago Rezende <thiago.manoel.rezende@gmail.com>"
LABEL version="0.1"

# System setup
RUN set -e \
    && apk add --no-cache gettext openjdk11 maven gradle nodejs npm \
    && addgroup -g 1000 spring \
    && adduser -u 1000 -S spring -G spring

# Copy entrypoint scripts
COPY docker-entrypoint.sh /
COPY docker-entrypoint.d /docker-entrypoint.d

# Mark the scritps as executable
RUN set -e \
    && chmod 755 /docker-entrypoint.sh \
    && find /docker-entrypoint.d -type f -iname "*.sh" -exec chmod 755 {} \;

# User
USER spring:spring

# User directories
RUN set -e \
    && mkdir -p ~/application \
    && mkdir -p ~/.m2

# Copy application files
# COPY . /home/spring/application

# Application files mount volume
VOLUME [ "/home/spring/application" ]

# Set workdir
WORKDIR /home/spring/application

# Maven dependencies cache [DEPRECATED]
# VOLUME [ "/home/spring/.m2" ]

# Get all maven dependencies [NEW BUILDKIT] [REQUIRES: maven-dependency-plugin]
# RUN --mount=type=cache,target=/home/spring/.m2 set -e \
#     && mvn -f app dependency:go-offline

# Get most of maven dependencies and cache them [NEW BUILDKIT]
# RUN --mount=type=cache,target=/home/spring/.m2 set -e \
#     && mvn -f app dependency:resolve

# Ports
EXPOSE 8080

# Set entrypoint script
ENTRYPOINT ["/docker-entrypoint.sh"]

# Initialization command
CMD [ "mvn", "-f", "app", "spring-boot:run" ]
