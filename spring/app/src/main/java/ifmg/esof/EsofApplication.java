package ifmg.esof;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.CommandLineRunner;

@SpringBootApplication
public class EsofApplication {

	public static void main(String[] args) {
		SpringApplication.run(EsofApplication.class, args);
	}
}
