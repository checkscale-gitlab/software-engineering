package ifmg.esof.model;

import java.util.Set;
import lombok.Data;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import javax.persistence.ManyToOne;
import javax.persistence.ManyToMany;

@Data
@Entity
@Transactional
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @ManyToMany
    Set<Order> orders;

    public Product(String name, float price, int quantity) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }

    @NotNull(message = "Name cannot be null.")
    private String name;

    @NotNull(message = "Price cannot be null.")
    private float price;

    @NotNull(message = "Quantity cannot be null.")
    private int quantity;

    protected Product() {}
}
